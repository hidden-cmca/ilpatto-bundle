<#assign wp=JspTaglibs[ "/aps-core"]>

<script nonce="<@wp.cspNonce />" src="https://cdn.jsdelivr.net/npm/react@17.0.1/umd/react.production.min.js" ></script>
<script nonce="<@wp.cspNonce />" src="https://cdn.jsdelivr.net/npm/react-dom@17.0.1/umd/react-dom.production.min.js" ></script>
<script nonce="<@wp.cspNonce />" src="<@wp.resourceURL />ilpatto-bundle/js/widgets/ilpatto-microfrontends/build.js" ></script>

<ilpatto-service-url value="http://metroca.mshome.net/cmca/ilpatto-microservice/0-1-3-snapshot" />
<ilpatto-gestione-aree ></ilpatto-gestione-aree>